import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import * as serviceWorker from './serviceWorker';
import 'bootstrap/dist/css/bootstrap.css';
import { Tab, Tabs, TabList, TabPanel } from 'react-tabs';
import "react-tabs/style/react-tabs.css";
var employeeData = [];


class MyForm extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            formControls: {
                employeeName: {
                    value: ''
                },
                age: {
                    value: ''
                },
                salary: {
                    value: ''
                },
            },
            renderJSON: {}
        };

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleChange = (event) => {
        const name = event.target.name;
        const value = event.target.value;

        this.setState({
            formControls: {
                ...this.state.formControls,
                [name]: {
                    ...this.state.formControls[name],
                    value
                }
            }
        });
    }

    handleSubmit(event) {
        let employeeDataNew = {
            employeeName: this.state.formControls.employeeName.value,
            age: this.state.formControls.age.value,
            salary: this.state.formControls.salary.value
        }
        employeeData.push(employeeDataNew);
        var data = JSON.stringify(employeeData);
        localStorage.setItem(employeeData, data);
        this.setState({
            renderJSON: localStorage.getItem(employeeData)
        });
        event.preventDefault();
    }

    render() {
        return (
            <div className="center d-flex justify-content-center color-background">
                <Tabs className="color-form shadow-xl p-3 mb-5 bg-white rounded">
                    <TabList >
                        <Tab>Cadastro</Tab>
                        <Tab>Funcionários</Tab>
                    </TabList>

                    <TabPanel>
                        <form onSubmit={this.handleSubmit} className="form ">
                            <h1 >Cadastro de Funcionários</h1>
                            <p>Insira o nome:</p>
                            <input
                                type='text'
                                name='employeeName'
                                value={this.state.formControls.employeeName.value}
                                onChange={this.handleChange}
                            />
                            <p>Insira a idade:</p>
                            <input
                                type='text'
                                name='age'
                                value={this.state.formControls.age.value}
                                onChange={this.handleChange}
                            />

                            <p>Insira o salário:</p>
                            <input
                                type='text'
                                name='salary'
                                value={this.state.formControls.salary.value}
                                onChange={this.handleChange}
                            />
                            <br />
                            <br />
                            <button type="input" className="btn btn-success" >Cadastrar</button>
                        </form>

                    </TabPanel>
                    <TabPanel>
                        <h1>Lista de Funcionários</h1>
                        <div>
                            {employeeData.map((employeeDetail, index) => {
                                return <div className="shadow-employee">
                                    <p className="left">Nome: {employeeDetail.employeeName}</p>
                                    <p className="left">Idade: {employeeDetail.age}</p>
                                    <p className="left">Salário: {employeeDetail.salary}</p>
                                </div>
                            })}
                        </div>
                    </TabPanel>
                </Tabs>

            </div>
        );
    }
}

ReactDOM.render(<MyForm />, document.getElementById('root'));